import java.time.LocalTime;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;
public class Change extends TimerTask {
    static int amountAliceAAPL = 10;
    static int amountAliceCOKE = 20;
    static int amountBobAAPL = 10;
    static int amountBobIBM = 20;
    static int amountCharlieCOKE = 300;
    static  int amountAAPL = 100;
    static  int amountCOKE = 1000;
    static int amountIBM = 200;
    static double priceAAPL = 141;
    static double priceCOKE = 387;
    static double priceIBM = 137;

    @Override
    public void run() {
        System.out.println("Running threads: " + new Date());
        changePrice();
        targetToBuyAlice1();
        targetToBuyAlice2();
        targetToBuyBob1();
        targetToBuyBob2();
        targetToBuyCharlie1();
        failedTargetBob1();
        failedTargetBob2();
        failedTargetCharlie1();
        failedTargetAlice1();
        failedTargetAlice2();
    }
    public static double plusMinusInt(double priceAAPL, int percent) {
        int p = ThreadLocalRandom.current().nextInt(2 * percent + 1) - percent;
        return priceAAPL * (100.0 + p) / 100.0; }
    public static double plusMinusInt2(double priceCOKE, int percent) {
        int p = ThreadLocalRandom.current().nextInt(2 * percent + 1) - percent;
        return priceCOKE * (100.0 + p) / 100.0; }
    public static double plusMinusInt3(double priceIBM, int percent) {
        int p = ThreadLocalRandom.current().nextInt(2 * percent + 1) - percent;
        return priceIBM * (100.0 + p) / 100.0; }
    public static synchronized void changePrice() {

        try {
            Thread.sleep(10000);
            for (int i = 0; i < 1; i++) {
                priceAAPL = plusMinusInt(priceAAPL, 3);
                priceCOKE = plusMinusInt2(priceCOKE, 3);
                priceIBM = plusMinusInt3(priceIBM, 3);
                System.out.println("The price AAPL has changed: " + priceAAPL);
                System.out.println("The price COkE has changed: " + priceCOKE);
                System.out.println("The price IBM has changed: " + priceIBM);
            }
        } catch (Exception s) {
            s.printStackTrace();
        }
    }
    public static synchronized void targetToBuyAlice1(){
        if (priceAAPL<=100){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                System.out.println(LocalTime.now() + " Alice's attempt to buy AAPL stock is successful. 10 shares were bought.");
                    System.out.println("AAPL shares left: " +  getAliceBuy1());}
            }catch (Exception r){
                r.printStackTrace();
            }
        }
    }
    public static int getAliceBuy1(){
        return amountAAPL - amountAliceAAPL;
}
    public static synchronized void targetToBuyAlice2(){
        if (priceCOKE >= 390){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Alice's attempt to buy COKE stock is successful. 20 shares were bought.");
                    System.out.println("COKE shares left: " + getAliceBuy2());}
            }catch (Exception r){
                r.printStackTrace();
            }
        }
    }
    public static int getAliceBuy2(){
        return amountCOKE - amountAliceCOKE;
    }
    public static synchronized void targetToBuyBob1(){
        if (priceAAPL<=140){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is successful. 10 shares were bought.");
                    System.out.println("AAPL shares left: " + getBobBuy1());}
            }catch (Exception r){
                r.printStackTrace();
            }
        }
    }
    public static int getBobBuy1(){
        return amountAAPL - amountBobAAPL;
    }
    public static synchronized void targetToBuyBob2(){
        if (priceIBM<=135){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is successful.20 shares were bought.");
                    System.out.println("IBM shares left: " + getBobBuy2());}
            }catch (Exception r){
                r.printStackTrace();
            }
        }
    }
    public static int getBobBuy2(){
        return amountIBM - amountBobIBM;
    }
    public static synchronized void targetToBuyCharlie1() {
        if (priceCOKE <= 370) {
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is successful. 300 shares were bought.");
                    System.out.println("COKE shares left: " + getCharlieBuy1());
                }
            } catch (Exception r) {
                r.printStackTrace();
            }
        }
    }
    public static int getCharlieBuy1(){
        return amountCOKE - amountCharlieCOKE;
    }
    public static synchronized void failedTargetAlice1(){
        if (priceAAPL>=100){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                System.out.println(LocalTime.now() + " Alice attempt to buy AAPL stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
}
    public static synchronized void failedTargetAlice2(){
        if (priceCOKE<=390){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Alice attempt to buy COKE stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetBob1(){
        if (priceAAPL>=140){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetBob2(){
        if (priceIBM>=135){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
    public static synchronized void failedTargetCharlie1(){
        if (priceCOKE>=370){
            try {
                Thread.sleep(5000);
                for (int i = 0; i < 1; i++){
                    System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is unsuccessful");}
            }catch (Exception l){
                l.printStackTrace();
            }
        }
    }
}

